def delay(packet):

    import time
    if packet['RTMPT']['header.csid'] == 7:
        try:
            delay = time.time() - packet.time
            packet.global_var('time',time.time())
            print("delay: ",round(delay,4))
            return packet
        except:
            print("next")
            packet.global_var('time', time.time())
            return packet